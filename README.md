# Magisk Panforte Condensed Font
1. Download [releases](https://github.com/muink/magisk-panforte-font/releases)
2. Add to Magisk Module
3. Reboot your phone
4. Enjoy~
