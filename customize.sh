#!/bin/sh

ui_print "- Searching in fonts.xml"
[[ -d /sbin/.magisk/mirror ]] && MIRRORPATH=/sbin/.magisk/mirror || unset MIRRORPATH
FILEPATH=/system/etc
FILE=fonts.xml
mkdir -p $MODPATH/$FILEPATH 2>/dev/null

ui_print "- Unzipping font files..."
FONTSPATH=/system/fonts
mkdir -p $MODPATH/$FONTSPATH 2>/dev/null
tar -xf $MODPATH/fonts.tar.xz -C $MODPATH/$FONTSPATH 2>/dev/null
rm -f $MODPATH/fonts.tar.xz


ui_print "- Installing fonts..."
RAWFONTS=$(sed -En '/<family name="sans-serif-condensed">/,/<\/family>/ {s|.*<font weight="400" style="normal">(.*).ttf<\/font>.*|\1|p}' $MIRRORPATH/$FILEPATH/$FILE|cut -f1 -d-)
NEWFONTS='PanforteCondensed'

# Just replace
for _font in `ls -1 $MODPATH/$FONTSPATH/*.ttf | sed -Ene 's|.*/([^/]+)|\1|' -e "s|${NEWFONTS}-||p"`; do
  ln -s $FONTSPATH/${NEWFONTS}-$_font $MODPATH/$FONTSPATH/${RAWFONTS}-$_font
done
ln -s $FONTSPATH/${NEWFONTS}-Regular.ttf $MODPATH/$FONTSPATH/${RAWFONTS}-Medium.ttf
ln -s $FONTSPATH/${NEWFONTS}-RegularItalic.ttf $MODPATH/$FONTSPATH/${RAWFONTS}-MediumItalic.ttf
